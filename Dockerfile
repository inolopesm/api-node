FROM node:16-alpine
COPY ./ ./
RUN yarn
EXPOSE 3000
CMD ["yarn", "start"]
