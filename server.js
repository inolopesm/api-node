const express = require('express')

const app = express()

app.get('/', (request, response) => {
  response.end('Hello World!')
})

app.listen(process.env.PORT ?? 3000)
